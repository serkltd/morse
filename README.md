## Morse Code Challenge

Takes an input file containing human readable text, and outputs obfuscated morse code into `files/<input_filename>_obfuscated.txt`.

Assumes an input file exists and has been placed in the `files/` directory.

## Usage

### Default
```
go run main.go
``` 
Input filename defaults to `input.txt`

### Alternative Usage
 ```
 go run main.go "<input_filename>.txt"
``` 
Uses command-line argument as input filename

## Examples

### Example 1
* Command:  `go run main.go`
* Input file contents: 
```
HELLO
I AM IN TROUBLE
```
* Output filename:  `input_obfuscated.txt`
* Output file contents: 
```
4|1|1A2|1A2|C
2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1
```

### Example 2
* Command:  `go run main.go "message.txt"`
* Input file contents: 
```
HELLO
I AM IN TROUBLE
```
* Output filename:  `message_obfuscated.txt`
* Output file contents: 
```
4|1|1A2|1A2|C
2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1
```

## Testing
Run `go test` to execute various test cases.
