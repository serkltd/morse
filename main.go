package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var (
	path       = "files/"
	inputFile  = "input.txt"
	alphabet   = map[string]string {
		"A": ".-",
		"B": "-...",
		"C": "-.-.",
		"D": "-..",
		"E": ".",
		"F": "..-.",
		"G": "--.",
		"H": "....",
		"I": "..",
		"J": ".---",
		"K": "-.-",
		"L": ".-..",
		"M": "--",
		"N": "-.",
		"O": "---",
		"P": ".--.",
		"Q": "--.-",
		"R": ".-.",
		"S": "...",
		"T": "-",
		"U": "..-",
		"V": "...-",
		"W": ".--",
		"X": "-..-",
		"Y": "-.--",
		"Z": "--..",
		"0": "-----",
		"1": ".----",
		"2": "..---",
		"3": "...--",
		"4": "....-",
		"5": ".....",
		"6": "-....",
		"7": "--...",
		"8": "---..",
		"9": "----.",
		".": ".-.-.-",
		",": "--..--",
	}
	oMap = map[int]string {
		1: "A",
		2: "B",
		3: "C",
		4: "D",
		5: "E",
		6: "F",
		7: "G",
	}
)

func main() {

	// Grab input filename from command-line argument if provided, defaults to "input.txt"
	if len(os.Args) > 1 {
		inputFile = os.Args[1]
	}

	// Obfuscate file
	obfuscateFile(inputFile)
}

// Obfuscate file containing human readable text
func obfuscateFile(inputFile string) {
	inputPath := path + inputFile

	// Open input file
	file, err := os.Open(inputPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Scan file
	scanner := bufio.NewScanner(file)

	var output []string

	// Scan line
	for scanner.Scan() {

		// Translate human readable line into morse code
		translation := translateLine(scanner.Text())

		// Obfuscate line of morse code
		obfuscation := obfuscateLine(translation)

		// Append to output array
		output = append(output, obfuscation)
	}

	// Build output filename
	outputFile := strings.TrimSuffix(inputFile, filepath.Ext(inputFile)) + "_obfuscated" + filepath.Ext(inputFile)
	outputFilepath := path + outputFile

	// Write output to file
	err = writeToFile(output, outputFilepath)
	if err != nil {
		log.Fatal(err)
	}
}

// Write contents to file
func writeToFile(contents []string, filepath string) error {

	// Create contents file
	f, err := os.Create(filepath)
	if err != nil {
		f.Close()
		return err
	}

	// Print to file line by line
	for _, v := range contents {
		_, err := fmt.Fprintln(f, v)
		if err != nil {
			return err
		}
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

// Split a line of morse code into an 2D array of words
func splitMorseIntoWords(morse string) [][]string {

	// Check to see if string contains any valid morse code
	isValidMorseCode := strings.ContainsAny(morse, ".-")
	if !isValidMorseCode {
		return [][]string{}
	}

	var arr [][]string

	// Split into words
	words := strings.Split(morse, "/")

	for _, word := range words {

		// Split into characters
		characters := strings.Split(word, "|")

		arr = append(arr, characters)
	}

	return arr
}

// Translate a line of human readable text into morse code
func translateLine(text string) string {
	words := strings.Split(text, " ")
	var morse []string
	for _, word := range words {
		morse = append(morse, translateWord(word))
	}
	return strings.Join(morse, "/")
}

// Translate a human readable word into morse code
func translateWord(text string) string {
	var arr []string

	for _, character := range text {
		// Convert character lookup to uppercase
		if val, ok := alphabet[strings.ToUpper(string(character))]; ok {
			arr = append(arr, val)
 		}
	}

	return strings.Join(arr, "|")
}

// Obfuscate a line of morse code
func obfuscateLine(morse string) string {
	words := splitMorseIntoWords(morse)

	obfuscation := ""

	for i := 0; i < len(words); i++ {
		if i > 0 {
			obfuscation = obfuscation + "/"
		}
		obfuscation = obfuscation + obfuscateWord(words[i])
	}

	return obfuscation
}

// Obfuscate a word of morse code
func obfuscateWord(word []string) string {
	var str string

	for i := 0; i < len(word); i++ {

		// Validate morse code character
		isValidMorseCode := regexp.MustCompile(`^[-.|]+$`).MatchString(word[i])
		if !isValidMorseCode {
			continue
		}

		if i > 0 {
			str = str + "|"
		}
		character := regexp.MustCompile("([^-]+)|([^.]+)").FindAllString(word[i], -1)
		str = str + obfuscateCharacter(character)
	}
	return str
}

// Obfuscate a character of morse code
func obfuscateCharacter(character []string) string {
	var str string

	for i := 0; i < len(character); i++ {

		// Validate morse code character
		isValidMorseCode := regexp.MustCompile(`^[-.]+$`).MatchString(character[i])
		if !isValidMorseCode {
			continue
		}

		if strings.Contains(character[i], ".") {
			str = str + strconv.Itoa(len(character[i]))
		}
		if strings.Contains(character[i], "-") {
			if val, ok := oMap[len(character[i])]; ok {
				str = str + val
			}
		}
	}
	return str
}