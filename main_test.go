package main

import (
	"fmt"
	"github.com/labstack/gommon/log"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

// Define models for test cases
type translateTestCase struct {
	value			string
	expected		string
	test			string
}

type obfuscateWordTestCase struct {
	value			[]string
	expected		string
	test			string
}

type obfuscateLineTestCase struct {
	value			string
	expected		string
	test			string
}

type obfuscateCharacterTestCase struct {
	value			[]string
	expected		string
	test			string
}

type splitMorseTestCase struct {
	value			string
	expected		[][]string
	test			string
}

type obfuscateFileTestCase struct {
	inputFilename  string
	inputContents  []string
	outputFilepath string
	expected       string
	test           string
}

type writeToFileTestCase struct {
	inputFilepath	string
	outputFilepath	string
	value			[]string
	expected		string
	test			string
}

// Define test cases
var (
	translateWordTestCases = []translateTestCase {
		{
			value:			"TROUBLE",
			expected:		"-|.-.|---|..-|-...|.-..|.",
			test:			"String of letters only (valid data)",
		},
		{
			value:			"trouble",
			expected:		"-|.-.|---|..-|-...|.-..|.",
			test:			"lowercase string of letters (valid data)",
		},
		{
			value:			"Trouble!",
			expected:		"-|.-.|---|..-|-...|.-..|.",
			test:			"Mixed case string containing character not in obfuscation map (valid and invalid data)",
		},
		{
			value:			"100",
			expected:		".----|-----|-----",
			test:			"String containing numbers (valid data)",
		},
		{
			value:			"I",
			expected:		"..",
			test:			"String containing single letter (valid data)",
		},
		{
			value:			"&",
			expected:		"",
			test:			"String containing invalid character (invalid data)",
		},
	}
	translateLineTestCases = []translateTestCase {
		{
			value:			"I AM IN TROUBLE",
			expected:		"../.-|--/..|-./-|.-.|---|..-|-...|.-..|.",
			test:			"Words separated by spaces (valid data)",
		},
		{
			value:			"I-AM-IN-TROUBLE",
			expected:		"..|.-|--|..|-.|-|.-.|---|..-|-...|.-..|.",
			test:			"Words separated by hyphens (valid and invalid data)",
		},
		{
			value:			"I",
			expected:		"..",
			test:			"One word (valid data)",
		},
		{
			value:			"I AM IN TROUBLE.",
			expected:		"../.-|--/..|-./-|.-.|---|..-|-...|.-..|.|.-.-.-",
			test:			"Words separated by spaces, with valid punctuation (valid data)",
		},
		{
			value:			"I AM IN TROUBLE!",
			expected:		"../.-|--/..|-./-|.-.|---|..-|-...|.-..|.",
			test:			"Words separated by spaces, with invalid punctuation (valid and invalid data)",
		},
	}
	splitMorseTestCases = []splitMorseTestCase {
		{
			value:			"../.-|--/..|-./-|.-.|---|..-|-...|.-..|.",
			expected:		[][]string{[]string{".."}, []string{".-", "--"}, []string{"..", "-."}, []string{"-", ".-.", "---", "..-", "-...", ".-..", "."}},
			test:			"Morse code for 'I AM IN TROUBLE' (valid data)",
		},
		{
			value:			"../.-|--/..|-./-|.-.|---|..-00|-...|.-..|.|5555",
			expected:		[][]string{[]string{".."}, []string{".-", "--"}, []string{"..", "-."}, []string{"-", ".-.", "---", "..-00", "-...", ".-..", ".", "5555"}},
			test:			"Valid morse mixed with invalid morse (valid and invalid data)",
		},
		{
			value:			"54354353454354",
			expected:		[][]string{},
			test:			"No valid morse code (invalid data)",
		},
	}
	obfuscateWordTestCases = []obfuscateWordTestCase {
		{
			value:			[]string{".."},
			expected:		"2",
			test:			"Morse code for 'I' (valid data)",
		},
		{
			value:			[]string{".-", "--"},
			expected:		"1A|B",
			test:			"Morse code for 'AM' (valid data)",
		},
		{
			value:			[]string{"-", ".-.", "---", "..-", "-...", ".-..", "."},
			expected:		"A|1A1|C|2A|A3|1A2|1",
			test:			"Morse code for 'TROUBLE' (valid data)",
		},
		{
			value:			[]string{"-", ".-.", "---", "..-", "-...", ".-..", ".5"},
			expected:		"A|1A1|C|2A|A3|1A2",
			test:			"Morse code for 'TROUBLE' including invalid morse code (valid and invalid data)",
		},
		{
			value:			[]string{"55", "22"},
			expected:		"",
			test:			"Numbers only (invalid data)",
		},
	}
	obfuscateLineTestCases = []obfuscateLineTestCase {
		{
			value:			".-|--",
			expected:		"1A|B",
			test:			"Single Word, AM (valid data)",
		},
		{
			value:			"../.-|--/..|-./-|.-.|---|..-|-...|.-..|.--..--/.--.|.-..|.|.-|...|./....|.|.-..|.--.",
			expected:		"2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1B2B/1B1|1A2|1|1A|3|1/4|1|1A2|1B1",
			test:			"Multiple words, with comma, and invalid exclamation - I AM IN TROUBLE, PLEASE HELP! (valid data)",
		},
		{
			value:			".-|--|.---4",
			expected:		"1A|B",
			test:			"Valid and invalid characters (valid and invalid data)",
		},
		{
			value:			"543543|5345|.-4354",
			expected:		"",
			test:			"No valid morse code (invalid data)",
		},
	}
	obfuscateCharacterTestCases = []obfuscateCharacterTestCase {
		{
			value:			[]string{".."},
			expected:		"2",
			test:			"Single letter, A (valid data)",
		},
		{
			value:			[]string{"../.-|--/..|-./-|.-.|---|..-|-...|.-..|."},
			expected:		"",
			test:			"Multiple words, I AM IN TROUBLE (valid data)",
		},
		{
			value:			[]string{"..", "4"},
			expected:		"2",
			test:			"Invalid characters (valid and invalid data)",
		},
		{
			value:			[]string{"..4"},
			expected:		"",
			test:			"Invalid characters (invalid data)",
		},
	}
	writeToFileTestCases = []writeToFileTestCase {
		{
			outputFilepath: "files/test_obfuscated.txt",
			value:			[]string{"4|1|1A2|1A2|C"},
			expected:		"4|1|1A2|1A2|C\n",
			test:			"One line (valid data)",
		},
		{
			outputFilepath: "files/test_obfuscated",
			value:			[]string{"4|1|1A2|1A2|C"},
			expected:		"4|1|1A2|1A2|C\n",
			test:			"One line, filename with no extension (valid data)",
		},
		{
			outputFilepath: "files/test_obfuscated.txt",
			value:			[]string{"4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1"},
			expected:		"4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n",
			test:			"Two lines (valid data)",
		},
		{
			outputFilepath: "files/test_obfuscated.txt",
			value:			nil,
			expected:		"",
			test:			"Nil data (valid data)",
		},
		{
			outputFilepath: "files/test_obfuscated.txt",
			value:			[]string{""},
			expected:		"\n",
			test:			"Empty string (valid data)",
		},
	}
	obfuscateFileTestCases = []obfuscateFileTestCase {
		{
			inputFilename:  "test1.txt",
			inputContents:  []string{"HELLO", "I AM IN TROUBLE"},
			outputFilepath: "files/test1_obfuscated.txt",
			expected:       "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n",
			test:           "Valid contents (valid data)",
		},
		{
			inputFilename:  "test2",
			inputContents:  []string{"HELLO", "I AM IN TROUBLE"},
			outputFilepath: "files/test2_obfuscated",
			expected:       "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n",
			test:           "Valid contents, input filename with no extension (valid data)",
		},
		{
			inputFilename:  "test3.txt",
			inputContents:  []string{"HELLO", "I AM IN TROUBLE!"},
			outputFilepath: "files/test3_obfuscated.txt",
			expected:       "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n",
			test:           "Mixed contents (valid and invalid data)",
		},
		{
			inputFilename:  "test4.txt",
			inputContents:  []string{"!"},
			outputFilepath: "files/test4_obfuscated.txt",
			expected:       "\n",
			test:           "Non-alphabet characters only (invalid data)",
		},
	}
)

func TestTranslateWord(t *testing.T) {
	for _, testCase := range translateWordTestCases {
		result := translateWord(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestTranslateLine(t *testing.T) {
	for _, testCase := range translateLineTestCases {
		result := translateLine(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestSplitCharacters(t *testing.T) {
	for _, testCase := range splitMorseTestCases {
		result := splitMorseIntoWords(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestObfuscateCharacter(t *testing.T) {
	for _, testCase := range obfuscateCharacterTestCases {
		result := obfuscateCharacter(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestObfuscateWord(t *testing.T) {
	for _, testCase := range obfuscateWordTestCases {
		result := obfuscateWord(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestObfuscateLine(t *testing.T) {
	for _, testCase := range obfuscateLineTestCases {
		result := obfuscateLine(testCase.value)
		assert.Equal(t, testCase.expected, result, testCase.test)
	}
}

func TestWriteToFile(t *testing.T) {

	for _, testCase := range writeToFileTestCases {

		// Write to file
		err := writeToFile(testCase.value, testCase.outputFilepath)
		if err != nil {
			log.Fatal(err)
		}

		// Assert file exists
		assert.FileExists(t, testCase.outputFilepath, "File does not exist")

		// Read contents back
		b, err := ioutil.ReadFile(testCase.outputFilepath)
		if err != nil {
			log.Fatal(err)
		}
		str := string(b)

		// Assert contents matches expected result
		assert.Equal(t, testCase.expected, str,"File contents does not match")

		// Cleanup
		err = os.Remove(testCase.outputFilepath)
		if err != nil {
			return
		}
	}

}

func TestObfuscateFile(t *testing.T) {
	for _, testCase := range obfuscateFileTestCases {

		inputFilepath := "files/" + testCase.inputFilename

		// Create test input file and write test data to file
		err := writeToFile(testCase.inputContents, inputFilepath)
		if err != nil {
			log.Fatal(err)
		}

		// Assert input file now exists
		assert.FileExists(t, inputFilepath, "File does not exist")

		// Obfuscate input file
		obfuscateFile(testCase.inputFilename)

		// Assert output file now exists
		assert.FileExists(t, testCase.outputFilepath, "File does not exist")

		// Read expected output file
		b, err := ioutil.ReadFile(testCase.outputFilepath)
		if err != nil {
			fmt.Print(err)
		}
		str := string(b)

		// Assert actual output contents matches expected output contents
		assert.Equal(t, testCase.expected, str,"File contents does not match")

		// Cleanup
		err = os.Remove(inputFilepath)
		if err != nil {
			return
		}

		err = os.Remove(testCase.outputFilepath)
		if err != nil {
			return
		}
	}
}